import Err from 'Error';

const browserType = {
    IPHONE: 'IPHONE',
    ANDROID: 'ANDROID',
    OTHER: 'OTHER'
}

//venmosdk://venmo.com/?client=ios&app_version=1.3.0&app_name=&app_id=&txn=charge&note=test&amount=10
function getDefaults() {
    return 
}

const _browserType = Symbol('browserType');
const _getQueryParms = Symbol('getQueryParms');
const _venmoEndpoint = Symbol('venmoEndpoint');
const _callVenmo = Symbol('callVenmo');

class Venmo {
    constructor() {
        if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
            this[_browserType] = browserType.IPHONE;
            this[_venmoEndpoint] = 'venmosdk://venmo.com/';
        }
        else if (/Android/i.test(navigator.userAgent)) {
            this[_browserType] = browserType.ANDROID;
            this[_venmoEndpoint] = 'venmosdk://paycharge';
        }
        else {
            this[_browserType] = browserType.OTHER;
            this[_venmoEndpoint] = 'https://venmo.com/';
        }
    }

    [_getQueryParms](amount, note) {
        const queryParms = {
            amount: amount,
            app_name: 'CC'//,
            //app_id: '1'
        }

        if (note != null) {
            queryParms.note = note;
        }
        
        if (this[_browserType] == browserType.IPHONE) {
            queryParms.client = 'ios';
            queryParms.app_version = '1.3.0';
        }
        else if(this[_browserType] == browserType.ANDROID) {
            queryParms.using_new_sdk = true;
            queryParms.app_local_id = 'abcd'
        }

        return queryParms;
    }

    [_callVenmo](queryParms) {
        let url = this[_venmoEndpoint] + '?';

        for (let parm in queryParms) {
            url += parm + '=' + encodeURIComponent(queryParms[parm]) + '&';
        }

        //remove the last &
        url = url.substring(0, url.length - 1);

        console.log(url);

        //http://stackoverflow.com/questions/13044805/how-to-check-if-an-app-is-installed-from-a-web-page-on-an-iphone
        if (this[_browserType] != browserType.OTHER) {
            var now = new Date().getTime();
            setTimeout(() => {
                if (new Date().getTime() - now < 500) {
                    Err.set('You need to have venmo installed');
                }
            }, 25);
        }

        window.location.href = url;
    }

    pay(amount, note = null) {
        const queryParms = this[_getQueryParms](amount, note);
        queryParms.txn = 'pay';
        
        this[_callVenmo](queryParms);
    }

    charge(amount, note = null) {
        const queryParms = this[_getQueryParms](amount, note);
        queryParms.txn = 'charge';
        
        this[_callVenmo](queryParms);
    }
}

export default new Venmo();