
export function getQueryParms() {
    let queryParms = window.location.search;
    const parmsMap = new Map();

    if (queryParms != '' && queryParms.length > 1) {
        queryParms = queryParms.substr(1).split('&')

        for (let queryParm of queryParms) {
            const [key, value] = queryParm.split('=');
            parmsMap.set(key, value);
        }

        //console.log(parmsMap);        
    }

    return parmsMap;
}

//a wrapper for submit functions to prevent the default action
export function preventDefault(callback) {
    return (e) => {
        if (e != null){
            e.preventDefault();
        }

        callback(e);

        return false;
    }
}