class Err {
    constructor() {
        this.errorDiv = document.getElementById('error');
        this.timeout = null;
    }

    set(message, timeout = 0) {
        this.errorDiv.textContent = message;
        this.errorDiv.style.visibility = 'visible';

        if (timeout > 0) {
            if (this.timeout != null) {
                clearTimeout(this.timeout);
            }

            this.timeout = setTimeout(() => {
                this.clear();
            }, timeout);
        }
    }

    clear() {
        this.timeout = null;

        this.errorDiv.textContent = '\u00a0';
        this.errorDiv.style.visibility = 'hidden';
    }

}

export default new Err();