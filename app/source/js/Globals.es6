const Globals = {
    ENVIRONMENT: '<environment>',
    FIXER_ENDPOINT: 'http://api.fixer.io/latest',
    CURRENCY_LAYER_ENDPOINT: 'http://apilayer.net/api/live',
    CURRENCY_LAYER_API_KEY: '<currency_layer_api_key>',

    isProd: function() {
        return Globals.ENVIRONMENT == 'PRODUCTION';
    }
}

export default Globals;