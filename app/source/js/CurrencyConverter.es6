import {preventDefault} from 'utils/Util';

import Currency from 'Currency';
import Venmo from 'Venmo';
import Err from 'Error';

const RECENT_CURRENCIES_STORAGE_KEY = 'recentCurrencies';
//ensure that we're only accepting expected values
const EXPRESSION_REGEX = /^[\s\d\.+\-\*\/\%\(\)]+$/;

const _round = Symbol('round');
const _setRecentCurrency = Symbol('setRecentCurrency');
const _getRecentCurrencies = Symbol('getRecentCurrencies');

//http://stackoverflow.com/questions/19373860/convert-currency-names-to-currency-symbol
const currency_symbols = {
    'USD': '$', // US Dollar
    'EUR': '€', // Euro
    'CRC': '₡', // Costa Rican Colón
    'GBP': '£', // British Pound Sterling
    'ILS': '₪', // Israeli New Sheqel
    'INR': '₹', // Indian Rupee
    'JPY': '¥', // Japanese Yen
    'KRW': '₩', // South Korean Won
    'NGN': '₦', // Nigerian Naira
    'PHP': '₱', // Philippine Peso
    'PLN': 'zł', // Polish Zloty
    'PYG': '₲', // Paraguayan Guarani
    'THB': '฿', // Thai Baht
    'UAH': '₴', // Ukrainian Hryvnia
    'VND': '₫', // Vietnamese Dong
};

export default class CurrencyConverter {
    constructor() {
        this.currencySymbol = document.getElementById('currencySymbol');
        this.numberInput = document.getElementById('numberInput');
        this.previewDiv = document.getElementById('convertedValue');
        this.currencySelector = document.getElementById('currencySelector');
        this.footer = document.getElementById('footer');

        this.numberInput.addEventListener('input', this.updatePreview.bind(this));
        this.currencySelector.addEventListener('input', this.updatePreview.bind(this));

        document.getElementById('pay').addEventListener('click', preventDefault(this.convert.bind(this, true)));
        document.getElementById('charge').addEventListener('click', preventDefault(this.convert.bind(this, false)));
    }

    update() {
        //$(this.currencySelector).selectmenu('disable');

        //clear out the selection box
        for (var i = 0; i < this.currencySelector.options.length; i++) {
            this.currencySelector.options[i] = null;
        };

        this.footer.textContent = 'Currency Updated: ' + moment(Currency.lastUpdated).format('lll');

        //console.log(Currency.getCurrencies());

        let currencyParent = this.currencySelector;
        const recentCurrencies = this[_getRecentCurrencies]();
        if (recentCurrencies.length > 0) {
            const optGroup = document.createElement('optgroup');
            optGroup.label = 'Recent Currencies';

            let first = true;
            for (let currency of recentCurrencies) {
                if (Currency.getRate(currency) != null) {
                    const option = document.createElement('option');
                    option.text = currency;
                    option.value = currency;
                    if (first) {
                        option.selected = true;
                        first = false;
                    }

                    optGroup.appendChild(option);
                }
            }

            this.currencySelector.appendChild(optGroup);

            currencyParent = document.createElement('optgroup');
            currencyParent.label = 'Currencies';

            this.currencySelector.appendChild(currencyParent);
        }

        for (let currency of Currency.getCurrencies()) {
            //console.log(currency)
            const option = document.createElement('option');
            option.text = currency;
            option.value = currency;
            if (currency == 'EUR' && recentCurrencies.length == 0) {
                option.selected = true;
            }

            currencyParent.appendChild(option);
        }

        //$(this.currencySelector).selectmenu('enable');
        //$(this.currencySelector).selectmenu('refresh', true);

        this.updatePreview();

        this.numberInput.focus();
    }

    [_round](input, funct = Math.round) {
        return funct(input * 100) / 100;
    }

    updatePreview() {
        //console.log('updatePreview')
        Err.clear();

        const originalValue = this.getInput();
        const currency = this.getCurrency();
        const usdValue = Currency.convert(currency, originalValue);

        this.currencySymbol.textContent = (currency in currency_symbols) ? currency_symbols[currency] : '\u00a0';
        this.previewDiv.textContent = this[_round](usdValue);
    }

    getInput() {
        //trim the whitespace and remove all the spaces
        let input = this.numberInput.value.trim();

        if (input == '') {
            return 0;
        }

        //check for an expression we can evaluate
        if (EXPRESSION_REGEX.test(input)) {
            try {
                console.log('"use strict"; return ' + input + ';');

                const evaluatedValue = new Function('"use strict"; return ' + input + ';')();

                if (!isNaN(evaluatedValue)) {
                    return evaluatedValue;
                }
            }
            catch(e) {
                console.error(e);
            }
        }

        Err.set('Couldn\'t evaluate your expression', 5000);
        return 0;
    }

    getCurrency() {
        return this.currencySelector.value;
    }


    [_setRecentCurrency](currency) {
        let recentCurrencies = this[_getRecentCurrencies]();

        //remove the currency if it's already in the array
        const oldIndex = recentCurrencies.indexOf(currency);
        if (oldIndex >= 0) {
            recentCurrencies.splice(oldIndex, 1);
        }

        recentCurrencies.unshift(currency);

        recentCurrencies = recentCurrencies.slice(0, 5);

        localStorage[RECENT_CURRENCIES_STORAGE_KEY] =  JSON.stringify(recentCurrencies);
    }

    [_getRecentCurrencies]() {
        if (!(RECENT_CURRENCIES_STORAGE_KEY in localStorage)) {
            return [];
        }

        let recentCurrencies = localStorage[RECENT_CURRENCIES_STORAGE_KEY];

        try {
            recentCurrencies = JSON.parse(recentCurrencies);
        }
        catch(e) {
            return [];
        }

        return recentCurrencies;
    }

    convert(pay) {
        const originalValue = this.getInput();

        if (originalValue <= 0) {
            Err.set('Please enter a number greater than 0', 5000);

            return;
        }
        else if (originalValue > 3000) {
            Err.set(originalValue + ' might be more than you can ' + ((pay) ? 'pay' : 'charge') + ' in venmo');
        }

        const currency = this.getCurrency();
        const currencySymbol = (currency in currency_symbols) ? currency_symbols[currency] : currency;
        const usdValue = Currency.convert(currency, originalValue);
        const venmoFunction = (pay) ? Venmo.pay : Venmo.charge;

        const roundedValue = this[_round](usdValue);

        //check if the usd value is too low
        if (roundedValue < 0.01) {
            Err.set('The USD value is less than 1 cent', 5000);

            return;
        }

        document.body.className = 'loading';

        this[_setRecentCurrency](currency);

        //I'm assuming that ios is doing something stupid with local storage (if I remove this line the recent currencies aren't saved)
        //so I'm just guessing we have to wait for 1 second before redirecting the page if I want localStorage to persist
        setTimeout(() => {
            document.body.className = '';
            venmoFunction.call(Venmo, roundedValue, 'Converted from ' + currencySymbol + ' ');
        }, 500);
    }
}