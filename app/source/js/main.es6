import Globals from 'Globals';
import {preventDefault} from 'utils/Util';

import Currency from 'Currency';
import CurrencyConverter from 'CurrencyConverter';

$(() => {
    // check if we need to disable console.log
    prepareConsole();

    setupCurrencyInput();

    //block submitting the form
    document.getElementById('form').addEventListener('submit', preventDefault((e) => {}));

    //update the currency
    Currency.update().then((response) => {
        const cc = new CurrencyConverter();

        cc.update()

        document.body.className = '';

        //console.log('ready', cc);
    }, (rejection) => console.log(rejection));
});


// disables the console for IE9 and for production environments
function prepareConsole() {
    if (!window.console || Globals.isProd()) {
        window.console = {
            log: function() {}
        };
    }
}


function setupCurrencyInput() {
    const currencyInputs = document.getElementsByClassName('currencyInput');

    for (let i = 0; i < currencyInputs.length; i++) {
        const currencyInput = currencyInputs[i];
        const realInput = currencyInput.getElementsByTagName('input')[0];

        realInput.addEventListener('focus', function() {
            currencyInput.className += ' selected';
        });

        realInput.addEventListener('blur', function() {
            currencyInput.className = currencyInput.className.replace('selected', '');
        })
    }
}