import Globals from 'Globals';

const SOURCE = 'USD';
const SOURCE_REGEX = new RegExp('^' + SOURCE);

const _currencyMap = Symbol('currencyMap');
const _updating = Symbol('updating');

const _updateConversions = Symbol('updateConversions');

const _updateCurrencyLayer = Symbol('updateCurrencyLayer');
const _updateFixer = Symbol('updateFixer');

class Currency {
    constructor() {
        this[_currencyMap] = null;
        this.lastUpdated = new Date(0);
    }

    [_updateCurrencyLayer]() {
        return new Promise((resolve, reject) => {
            $.ajax({
                method: 'GET',
                url: Globals.CURRENCY_LAYER_ENDPOINT + '?access_key=' + Globals.CURRENCY_LAYER_API_KEY + '&source=' + SOURCE,
                jsonp: 'callback',
                dataType: 'json'
            }).done((data) => {
                //console.log(data);

                if (data.success) {
                    this[_currencyMap] = new Map();
                    this.lastUpdated = new Date(data.timestamp * 1000);

                    for (let currency in data.quotes) {
                        const rate = data.quotes[currency];
                        currency = currency.toUpperCase().replace(SOURCE_REGEX, '');

                        this[_currencyMap].set(currency, rate);
                    }

                    resolve()
                }
                else {
                    reject('An error occured while updating the currencies');
                }
            }).fail((jqXHR) => {
                reject('An error occured while updating the currencies');
            });
        });
    }

    [_updateFixer]() {
        return new Promise((resolve, reject) => {
            $.ajax({
                method: 'GET',
                url: Globals.FIXER_ENDPOINT + '?base=' + SOURCE,
                jsonp: 'callback',
                dataType: 'jsonp'
            }).done((data) => {
                this[_currencyMap] = new Map();
                this.lastUpdated = new Date(data.date);

                for (let currency in data.rates) {
                    const rate = data.rates[currency];
                    currency = currency.toUpperCase();

                    this[_currencyMap].set(currency, rate);
                }

                resolve()
            }).fail((jqXHR) => {
                reject('An error occured while updating the currencies');
            });
        });
    }

    update() {
        //if we're not in production use the free api
        if (!Globals.isProd()) {
            return this[_updateFixer]();
        }

        //try to get the currency info from 2 apis
        return new Promise((resolve, reject) => {
            this[_updateCurrencyLayer]().then(resolve, () => {
                this[_updateFixer]().then(resolve, reject);
            });
        });
    }

    getCurrencies() {
        if (this[_currencyMap] != null) {
            const resultArray = [];

            for (let currency of this[_currencyMap].keys()) {
                resultArray.push(currency);
            }

            return resultArray.sort();
        }
        
        return null;
    }

    getRate(currency) {
        if (this[_currencyMap] != null) {
            return this[_currencyMap].get(currency.toUpperCase());
        }
        
        return null;
    }

    //converts the input to USD
    convert(currency, value) {
        const rate = this.getRate(currency);

        return value * (1 / rate);
    }
    
}

export default new Currency()
