# Venmo Currency #

### How do I get set up? ###

1. Make sure node is installed
2. Make sure grunt is installed `npm install -g grunt-cli`
3. Download the necessary node modules `npm install`
4. Run Grunt `grunt`
5. Open the application `http://localhost:9001/app/`