//load es6 files as regular javascript
require('babel/register');
var fs = require('fs');
var babel = require('babel');
var compiler = require('superstartup-closure-compiler');


var appDir = 'app';
var publishDir = appDir + '/build';
var srcJsDir = appDir + '/source/js';
var jsFolder = 'js';
var cssFolder = 'css';
var libFolder = 'lib';
var imagesFolder = 'images';

function getPath(path) {
    var dirs = [jsFolder, cssFolder, libFolder, imagesFolder];

    function buildPaths(destination, dir) {
        destination[dir] = destination.root + '/' + dir;
    }

    var output = {root: path};
    dirs.forEach(buildPaths.bind(null, output));

    output.source = {root: path + '/source'};
    dirs.forEach(buildPaths.bind(null, output.source))

    return output;
}

var devHost = 'localhost';
var devPort = 9001;

module.exports = function(grunt) {
    var config = {
        connect: {
            server: {
                options: {
                    debug: true,
                    port: devPort,
                    hostname: devHost
                }
            }
        },
        watch: {
            scripts: {
                files: [getPath(appDir).source.js + '/**/*.es6'],
                tasks: ['babel:watch', 'setupGlobals:develop'],
                options: {
                    spawn: false
                }
            }
        },
        babel: {
            options: {
                sourceMap: false,
                modules: 'amd',
                sourceMaps: false,
                externalHelpers: true
            },
            watch: {
                files: {}
            },
            develop: {
                expand: true,
                cwd: getPath(appDir).source.js + '/',
                src: '**/*.es6',
                dest: getPath(appDir).js + '/',
                ext: '.js'
            },
            build: {
                expand: true,
                cwd: getPath(appDir).source.js + '/',
                src: '**/*.es6',
                dest: getPath(publishDir).js,
                ext: '.js'
            },
            publish: {
                expand: true,
                cwd: getPath(appDir).source.js + '/',
                src: '**/*.es6',
                dest: getPath(publishDir).source.js,
                ext: '.js'
            }
        },
        
        requirejs: {
            options: {
                name: 'main',
                optimize: 'none'
            },
            publish: {
                options: {
                    baseUrl: getPath(publishDir).source.js,
                    //paths: {
                    //    global: '../global'
                    //    core: './core'
                    //}
                    name: 'main',
                    out: './' + getPath(publishDir).js + '/main.require.js',
                    optimize: 'none'
                }
            }
        },
        closureCompiler: {
            options: {
                compilerFile: compiler.getPath(),
                execOpts: {
                   maxBuffer: 999999 * 1024
                }
            },
            publish: {
                files: [
                    {
                        expand: true,
                        src: [getPath(publishDir).js + '/**/*.require.js'],
                        ext: '.js'
                    }
                ]
            }
        },
        copy: {
            publish: {
                expand: true,
                cwd: getPath(appDir).root,
                src: ['index.html', cssFolder + '/**/*', libFolder + '/**/*', imagesFolder + '/**/*'],
                dest: getPath(publishDir).root
            }
        },
        clean: {
            publish: [publishDir + '/**/*'],
            develop: [getPath(appDir).js + '/**/*'],
            temp: [getPath(publishDir).source.root, getPath(publishDir).js + '/**/*.require.js']
        }
    };
    grunt.initConfig(config);

    grunt.event.on('watch', function(action, filepath) {
        //crudely get the destination directory and fix the extension
        filename = filepath.replace(getPath(appDir).source.js, getPath(appDir).js).replace(/\.es6$/, '.js')

        obj = {};
        obj[filename] = filepath;

        console.log(obj);

        grunt.config('babel.watch.files', obj);
    });

    function setupGlobals(globalsPath, environment) {
        var path = globalsPath + '/Globals.js';
        console.log(path)

        var keys = fs.readFileSync('keys.json', 'utf-8');
        keys = JSON.parse(keys);

        //this expects Globals.js to exist already
        var data = fs.readFileSync(path, 'utf-8');
        data = data.replace('<environment>', environment);
        data = data.replace('<currency_layer_api_key>', keys.CURRENCY_LAYER_API_KEY);

        fs.writeFileSync(path, data, 'utf-8');
    }

    grunt.registerTask('setupGlobals', function(type) {
        if (type == 'develop') {
            setupGlobals(getPath(appDir).js, 'DEBUG', false);
        }
        else if (type == 'build') {
            setupGlobals(getPath(publishDir).js, 'DEBUG', true);
        }
        else {
            setupGlobals(getPath(publishDir).source.js, 'PRODUCTION', true);
        }
    });

    grunt.registerTask('develop', 'This task prepares the app for development', function() {
        grunt.task.run('connect', 'clean:develop', 'babel:develop', 'setupGlobals:develop', 'watch:scripts');
    });

    grunt.registerTask('build', 'Build a dev version of the app', function() {
        grunt.task.run(
            'clean:publish',
            'babel:build',
            'setupGlobals:build',
            'clean:temp',
            'copy:publish'
        );
    });

    grunt.registerTask('publish', 'Publish the app', function() {
        grunt.task.run(
            'clean:publish',
            'babel:publish',
            'setupGlobals:publish',
            'requirejs:publish',
            'closureCompiler',
            'clean:temp',
            'copy:publish'
        );
    });

    grunt.registerTask('default', ['develop']);

    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-closure-tools');
};